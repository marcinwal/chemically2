﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Element : MonoBehaviour {


    private GameObject _element;


    bool isMoving = false;
    public int atomicNumber;
    public float speed = 2.0f;

    private int _posx;
    private int _posy;
    private int _directionH = 0;
    private int _directionV = 0;

    private void Start()
    {

    }

    private void FixedUpdate()
    {
        if (isMoving)
        {
            //transform.Translate((transform.position.x + _directionH) * speed * Time.deltaTime,
            //                    (transform.position.y + _directionV) * speed * Time.deltaTime,
            //                    0, Space.World);
            transform.Translate( _directionH * speed * Time.deltaTime,
                                 _directionV * speed * Time.deltaTime,
                                 0, Space.World);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("hitting:" + collision.gameObject.name);
        Debug.Log("position:" + transform.position.ToString());
        isMoving = false;

    }

    private void Update()
    {
        float horizontal, vertical;

        if (Input.anyKey && !isMoving)
        {
            //transform.Translate(0, -100 * speed * Time.deltaTime, 0);
            //transform.Translate(0, -1000 * Time.deltaTime * speed,0, Space.World);
            //transform.Translate(0, - speed * Time.deltaTime, 0);
            //transform.Rotate(transform.position, 10.0f);
            //Debug.Log("move position:" + transform.position.ToString());
            //_rigidBody.velocity = new Vector3(0, transform.position.y -200, 0) * Time.deltaTime * speed;
            _directionV = -10;
            isMoving = true;
        }

        if(Input.touches.Length != 0)
        {
            _directionH = -10;
            isMoving = true;
        }

        //if(Input.touches.Length != 0 )
        //{
        //    Vector2 touchedPosition = Input.GetTouch(0).position;
        //    Vector3 targetPosition = new Vector3(transform.position.x, transform.position.y - 100, transform.position.z);

        //    horizontal = Input.GetAxis("Horizontal");
        //    vertical = Input.GetAxis("Vertical");

        //    Debug.Log("Touched");
        //    transform.Translate(horizontal * speed * Time.deltaTime, vertical * speed * Time.deltaTime, 0);
        //}
    }
}
