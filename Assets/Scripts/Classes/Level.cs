﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Level : MonoBehaviour {

    public enum hardness
    {
        easy = 0,
        medium = 1,
        hard = 2
    }

    public string[] pattern;
    public string formulaName;
    public string formula;
    public Material frame;
    public Material background;



}
